#!/bin/sh
# Fibonacci layout for herbstluftwm

# Get the current screen dimensions
screen_width=$(herbstclient monitor_rect | awk '{print $3}')
screen_height=$(herbstclient monitor_rect | awk '{print $4}')

# Get the number of clients
clients=$(herbstclient list_clients | wc -l)

# Define the initial position and dimensions
x=0
y=0
width=$screen_width
height=$screen_height

# Arrange clients in Fibonacci spiral
for i in $(seq 1 $clients); do
    herbstclient resize $width $height move $x $y

    # Update position and dimensions for the next window
    if [ $(($i % 2)) -eq 1 ]; then
        height=$(($height * 61 / 100))
        y=$(($y + $height))
    else
        width=$(($width * 61 / 100))
        x=$(($x + $width))
    fi
done
